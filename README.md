# chelevra-syntax theme

Simple conversion of my favorite sublime theme, ["chelevra"](https://github.com/jhermsmeier/chelevra.tmtheme). Doesn't have a lot of colors to distinguish lots of things, but overall cleaner in my opinion.

![A screenshot of your theme](http://i.imgur.com/Ho7UxLf.png)

I'll take it down if the owner wants me to.
